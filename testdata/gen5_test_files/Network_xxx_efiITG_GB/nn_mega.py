# Dummy file to test quickslicer scripting interface
from qlknn.models.ffnn import QuaLiKizNDNN
from IPython import embed
from pathlib import Path
nns = {}
nn_json_path = (Path(__file__) / "../nn.json").resolve()
nn = QuaLiKizNDNN.from_json(f"{nn_json_path}")
nn.label = "pretty_label"
nns[nn.label] = nn
slicedim = "Ati"
style = "mono"
