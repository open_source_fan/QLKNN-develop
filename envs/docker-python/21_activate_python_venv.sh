#!/bin/sh
# Create a python venv with access to the globally installed packages.
# This links to setuptools, pip and python, pkg_resources, easy_install as well
$PYTHON -m venv --system-site-packages $PYTHONTOOLS_VENV_PATH
source $PYTHONTOOLS_VENV_PATH/bin/activate
# Simulate Python3.9 --upgrade-deps flag
#   --upgrade-deps        Upgrade core dependencies: pip setuptools to the
#                         latest version in PyPI
$PIP install --force-reinstall --upgrade pip setuptools wheel

export VENV_SITE_PACKAGES=$VIRTUAL_ENV/lib/python`python --version | cut -d' ' -f2 | cut -d'.' -f1-2`/site-packages/
export SYSTEM_PACKAGES=/usr/lib/python3/dist-packages/ # Add system packages
# Give venv PYTONPATH and PATH precedence over pre-defined env ones
export PATH="$VIRTUAL_ENV/bin:$PATH"
export PYTHONPATH=${PYTHONPATH:-""}
export PYTHONPATH="$VENV_SITE_PACKAGES:$SYSTEM_PACKAGES:$PYTHONPATH"
