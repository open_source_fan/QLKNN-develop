#!/bin/bash
# Build the IMASPy package and run tests on an ITER SDCC-like environment

# Script boilerplate
[[ "${BASH_SOURCE[0]}" != "${0}" ]] && docker_python_dir=$(dirname ${BASH_SOURCE[0]}) || docker_python_dir=$(dirname $0)  # Determine script dir even when sourcing

old_bash_state="$(get_bash_state "$old_bash_state")"
set -xeuf -o pipefail # Set default script debugging flags

###############
# Script body #
###############

. $docker_python_dir/00_prepare_venv_path.sh
. $docker_python_dir/20_setenv_pure_python.sh
. $docker_python_dir/21_activate_python_venv.sh
. $docker_python_dir/22_install_qualikiz_tools.sh

set +xeuf +o pipefail # Set default manual debugging flags
