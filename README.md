# QLKNN

*A collection of tools to create QuaLiKiz Neural Networks.*

Read the [GitLab Pages](https://qualikiz-group.gitlab.io/QLKNN-develop/) and [wiki](https://gitlab.com/qualikiz-group/QuaLiKiz/-/wikis/QLKNN/QLKNN-overview) for more information.
